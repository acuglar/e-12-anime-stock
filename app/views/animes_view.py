from flask import Blueprint, request, jsonify
from http import HTTPStatus
from psycopg2 import errors
from app.services import AnimesTable


bp_animes = Blueprint('bp_animes', __name__, url_prefix='/api')


@bp_animes.route('/animes', methods=['GET', 'POST'])
def get_create():
    animes = AnimesTable()
    
    if request.method == 'POST':
        data = request.get_json()
        
        try:
            animes.create_anime(data)
            return data, HTTPStatus.CREATED

        except errors.UniqueViolation as _:
            return {"error": f'Anime \'{data["anime"]}\' already exists'}, HTTPStatus.UNPROCESSABLE_ENTITY

    try:
        data = animes.get_all()
        if not data:
            return {"no data": data}, HTTPStatus.OK
        
        return jsonify(data)
            
    except errors.UndefinedTable as _:
        return {"no table": []}, HTTPStatus.OK
    
    

@bp_animes.route('/animes/<int:anime_id>')
def filter(anime_id):
    animes = AnimesTable()
    
    try:
        return {f'Anime id \'{anime_id}\'': animes.filter_anime_by_id(anime_id)}, HTTPStatus.OK
    
    except:
        return {"no id": {}}, HTTPStatus.NOT_FOUND



@bp_animes.route('/animes/<int:anime_id>', methods=['PATCH'])
def update(anime_id):
    animes = AnimesTable()

    try:
        animes.filter_anime_by_id(anime_id)

    except:
        return {"no id": {}}, HTTPStatus.NOT_FOUND

    data = request.get_json()
    animes.update_anime(data, anime_id)

    return data, HTTPStatus.OK

    

@bp_animes.route('/animes/<int:anime_id>', methods=['DELETE'])
def delete(anime_id):
    animes = AnimesTable()
    query = animes.delete_anime(anime_id)
    
    if query:
        return "no content", HTTPStatus.NO_CONTENT

    return {"no id": {}}, HTTPStatus.NOT_FOUND

    
