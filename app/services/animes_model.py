from . import conn_cur, end_conn_cur
from http import HTTPStatus


class AnimesTable:
    table_fields = ['id', 'anime', 'release_data', 'seasons']
    required_fields = ['anime', 'release_date', 'seasons']

    @staticmethod
    def create_table() -> None:
        conn, cur = conn_cur()

        cur.execute(
            """
                CREATE TABLE IF NOT EXISTS animes 
                    (
                        id bigserial PRIMARY KEY,
                        anime VARCHAR(100) NOT NULL UNIQUE,
                        release_date DATE NOT NULL, 
                        seasons INTEGER NOT NULL
                    );
            """
        )

        end_conn_cur(conn, cur)


    def check_fields(self, data: dict) -> tuple:
        correct_fields = [i for i in data if i in self.required_fields]
        missing_fields = [i for i in self.required_fields if i not in data]
        
        return correct_fields, missing_fields


    def create_anime(self, data: dict) -> dict:
        conn, cur = conn_cur()
        self.create_table()

        correct_fields, missing_fields = self.check_fields(data)

        if missing_fields:
            return {
                "correct fields": correct_fields,
                "missing fields": missing_fields 
                }, HTTPStatus.UNPROCESSABLE_ENTITY

        data['anime'] = data['anime'].title()

        cur.execute(
            """
                INSERT INTO animes
                    (anime, release_date, seasons)
                VALUES
                    (%(anime)s, %(release_date)s, %(seasons)s)
                RETURNING *
            """,
            data
        )

        query = cur.fetchone()

        end_conn_cur(conn, cur)

        return dict(zip(self.table_fields, query))


    def get_all(self) -> dict:
        conn, cur = conn_cur()

        cur.execute("SELECT * FROM animes")

        query = cur.fetchall()

        end_conn_cur(conn, cur)

        return [dict(zip(self.table_fields, anime)) for anime in query]


    def filter_anime_by_id(self, anime_id: int) -> dict:
        conn, cur = conn_cur()

        cur.execute(
            """
                SELECT * FROM animes 
                    WHERE id = %(anime_id)s
            """, 
            {"anime_id": anime_id}
        )

        query = cur.fetchone()

        end_conn_cur(conn, cur)

        return dict(zip(self.table_fields, query))

    
    def delete_anime(self, anime_id: int) -> str:
        conn, cur = conn_cur()

        cur.execute(
            """
                DELETE FROM animes
                    WHERE id = %(anime_id)s
                    RETURNING *
            """,
            {"anime_id": anime_id},
        )

        query = cur.fetchone()

        end_conn_cur(conn, cur)

        return query


    def update_anime(self, data: dict, anime_id: int) -> dict:
        conn, cur = conn_cur()

        correct_fields, missing_fields = self.check_fields(data)

        if missing_fields:
            return {
                "correct fields": correct_fields,
                "missing fields": missing_fields 
                }, HTTPStatus.UNPROCESSABLE_ENTITY

        data['anime'] = data['anime'].title()
        data["anime_id"] = anime_id

        cur.execute(
            """
                UPDATE animes
                SET anime = %(anime)s, release_date = %(release_date)s, seasons = %(seasons)s
                    WHERE id = %(anime_id)s
                RETURNING *
            """,
            data,
        )

        query = cur.fetchone()

        end_conn_cur(conn, cur)
        
        return dict(zip(self.table_fields, query))